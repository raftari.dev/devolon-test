## Run Project with Docker
First remember to install docker on your machine.
Secondly you need to install docker-compose.

The project uses Laravel Sail, which is based on Docker.
You can run the project by using the following command:
```
cp .env.example .env
php artisan key:generate
php artisan sail
```

It is recommended to add following alias to your command line:
```
alias sail='./vendor/bin/sail'
```

Then you can interact with the project's container using artisan commands like this:

```
sail artisan migrate:refresh --seed
```


## Test project

There is a `.env.testing` file for testing the project. If you wanted to test the project with PHPUnit, you can run the following command:
```
touch database/database.sqlite
php artisan test
```
## Apis
For Apis, there's a postman collection to use:
```
https://www.getpostman.com/collections/46fd57ace24acd5be854
```
and following online document:
```
https://documenter.getpostman.com/view/2737985/Tzz7PJK9
```

## Load Data From Json Files
You can use these two commands to load json files into the database.
```
php artisan load:articles {file_name}
php artisan load:products {file_name}
```

Example files are available in the `database/fixtures` folder and you can use the following command to load them:
```
php artisan load:articles database/fixtures/articles.json
php artisan load:products database/fixtures/products.json
```

** please note: **
- `database/fixtures/articles.json` contains ids for articles, so if there are any duplicates, you receive an error. In otherwords if you had run seeder before, you probably run into an error
- load `articles.json` and `products.json` in the same order, otherwise you will receive an error because of foreign key constraints

## Optimized Production Plan Algorithm
It was asked to get all products based on articles we have, in a way that It maximizes the profit of selling products.
This problem somehow is similar to the knapsack problem but have some differences. The problem is to maximize the profit of selling products and making some of products prevents from creating others and just like the knapsack problem, the problem is np-hard.
In order to solve the problem I wrote an algorithm just like knapsack. I simply used a tree and a DFS to solve the problem.
At each tree's depth, branches are created based on how many product of kind `i` can be created, where `i` is the depth node exists.
This algorithm has a O(2^n) time complexity and in order to make it faster, I used a pruning mechanism. It is possible to make it faster by using some dynamic programming approach and avoid duplicate calculations but I didn't implemented in my algorithm.

The answer can be get from below URL:
```
{base_url}/api/warehouse
```

or by running the following command:
```
php artisan warehouse:production-plan
```
