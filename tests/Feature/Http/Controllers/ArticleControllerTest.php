<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Article;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\ArticleController
 */
class ArticleControllerTest extends TestCase
{
    use AdditionalAssertions;
    use RefreshDatabase;
    use WithFaker;

    /**
     * @test
     */
    public function index_behaves_as_expected()
    {
        $articles = Article::factory()->count(3)->create();

        $response = $this->get(route('article.index'));

        $response->assertOk();
        $response->assertJsonStructure([]);
    }

    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\ArticleController::class,
            'store',
            \App\Http\Requests\ArticleStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves()
    {
        $name = $this->faker->name;
        $stock = $this->faker->numberBetween(0, 10000);

        $response = $this->post(route('article.store'), [
            'name' => $name,
            'stock' => $stock,
        ]);

        $articles = Article::query()
            ->where('name', $name)
            ->where('stock', $stock)
            ->get();
        $this->assertCount(1, $articles);
        $article = $articles->first();

        $response->assertCreated();
        $response->assertJsonStructure([]);
    }

    /**
     * @test
     */
    public function show_behaves_as_expected()
    {
        $article = Article::factory()->create();

        $response = $this->get(route('article.show', $article));

        $response->assertOk();
        $response->assertJsonStructure([]);
    }

    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\ArticleController::class,
            'update',
            \App\Http\Requests\ArticleUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_behaves_as_expected()
    {
        $article = Article::factory()->create();
        $name = $this->faker->name;
        $stock = $this->faker->numberBetween(0, 10000);

        $response = $this->put(route('article.update', $article), [
            'name' => $name,
            'stock' => $stock,
        ]);

        $article->refresh();

        $response->assertOk();
        $response->assertJsonStructure([]);

        $this->assertEquals($name, $article->name);
        $this->assertEquals($stock, $article->stock);
    }
}
