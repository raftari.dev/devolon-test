<?php

namespace Tests\Feature\Console\Commands;

use App\Models\Product;
use Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * @see \App\Console\Commands\LoadProducts
 */
class LoadProductsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @group load-products
     */
    public function load_products_from_json()
    {
        Artisan::call('load:articles', ['fixture' => 'database/fixtures/articles.json']);

        $this->artisan('load:products', ['fixture' => 'database/fixtures/products.json'])
         ->expectsOutput(__('messages.loaded_successfully', ['entity' => 'Products']))
         ->assertExitCode(0);



        // NOTE: these tests are based on the fixture files
        //       database/fixtures/articles.json
        //       database/fixtures/products.json
        //       if the files are changed, the tests will need to be updated
        $products = Product::count();
        $this->assertEquals(2, $products);

        $productExists = Product::query()
            ->where('name', 'Dining Chair')
            ->where('price', 1000)
            ->exists();
        $this->assertTrue($productExists);

        $product = Product::whereName('Dinning Table')->first();
        $productAttachedArticlesCount = $product->articles()->count();
        $this->assertEquals(3, $productAttachedArticlesCount);
    }

    /**
     * @test
     * @group load-products
     */
    public function show_correct_error_when_loading_from_wrong_address()
    {
        $wrongPath = 'database/fixtures/products1.json';
        $this->artisan('load:products', ['fixture' => $wrongPath])
         ->expectsOutput("file_get_contents($wrongPath): Failed to open stream: No such file or directory")
         ->assertExitCode(1);
    }

    /**
     * @test
     * @group load-products
     */
    public function show_correct_error_when_file_is_not_json_formatted()
    {
        $poorlyFormattedFile = 'tests/Feature/Console/Commands/products-poorly-formatted.json';
        $this->artisan('load:products', ['fixture' => $poorlyFormattedFile])
         ->expectsOutput(__('messages.wrong_formatted_json'))
         ->assertExitCode(1);
    }

    /**
     * @test
     * @group load-products
     */
    public function show_correct_error_when_json_file_doesnt_have_products()
    {
        $badFile = 'tests/Feature/Console/Commands/products-json-file-without-products-key.json';
        $this->artisan('load:products', ['fixture' => $badFile])
         ->expectsOutput(__('messages.key_is_missing', ['key' => 'products']))
         ->assertExitCode(1);
    }

    /**
     * @test
     * @group load-products
     */
    public function show_correct_error_when_products_in_json_file_is_not_array()
    {
        $badFile = 'tests/Feature/Console/Commands/products-json-file-with-wrong-type-products-key.json';
        $this->artisan('load:products', ['fixture' => $badFile])
         ->expectsOutput(__('messages.wrong_key_type', ['key' => 'products', 'type' => 'array']))
         ->assertExitCode(1);
    }
}
