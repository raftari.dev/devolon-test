<?php

namespace Tests\Feature\Console\Commands;

use App\Models\Article;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * @see \App\Console\Commands\LoadArticles
 */
class LoadArticlesTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @group load-articles
     */
    public function load_articles_from_json()
    {
        $this->artisan('load:articles', ['fixture' => 'database/fixtures/articles.json'])
         ->expectsOutput(__('messages.loaded_successfully', ['entity' => 'Articles']))
         ->assertExitCode(0);



        // NOTE: these tests are based on the fixture file
        //       database/fixtures/articles.json
        //       if the file is changed, the tests will need to be updated
        $articles = Article::count();
        $this->assertEquals(4, $articles);

        $articleExists = Article::query()
            ->where('name', 'leg')
            ->where('stock', 12)
            ->exists();
        $this->assertTrue($articleExists);

        $articleExists = Article::find(4);
        $this->assertEquals('table top', $articleExists->name);
    }

    /**
     * @test
     * @group load-articles
     */
    public function show_correct_error_when_loading_from_wrong_address()
    {
        $wrongPath = 'database/fixtures/articles1.json';
        $this->artisan('load:articles', ['fixture' => $wrongPath])
         ->expectsOutput("file_get_contents($wrongPath): Failed to open stream: No such file or directory")
         ->assertExitCode(1);
    }

    /**
     * @test
     * @group load-articles
     */
    public function show_correct_error_when_file_is_not_json_formatted()
    {
        $poorlyFormattedFile = 'tests/Feature/Console/Commands/articles-poorly-formatted.json';
        $this->artisan('load:articles', ['fixture' => $poorlyFormattedFile])
         ->expectsOutput(__('messages.wrong_formatted_json'))
         ->assertExitCode(1);
    }

    /**
     * @test
     * @group load-articles
     */
    public function show_correct_error_when_json_file_doesnt_have_articles()
    {
        $badFile = 'tests/Feature/Console/Commands/articles-json-file-without-articles-key.json';
        $this->artisan('load:articles', ['fixture' => $badFile])
         ->expectsOutput(__('messages.key_is_missing', ['key' => 'articles']))
         ->assertExitCode(1);
    }

    /**
     * @test
     * @group load-articles
     */
    public function show_correct_error_when_articles_in_json_file_is_not_array()
    {
        $badFile = 'tests/Feature/Console/Commands/articles-json-file-with-wrong-type-articles-key.json';
        $this->artisan('load:articles', ['fixture' => $badFile])
         ->expectsOutput(__('messages.wrong_key_type', ['key' => 'articles', 'type' => 'array']))
         ->assertExitCode(1);
    }
}
