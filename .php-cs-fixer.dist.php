<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude('vendor')
    // ->notPath('src/Symfony/Component/Translation/Tests/fixtures/resources.php')
    ->in(__DIR__)
;

$config = new PhpCsFixer\Config();
return $config->setRules(
    [
            '@PSR12' => true,
            'concat_space' => ['spacing' => 'one'],
            'array_syntax' => ['syntax' => 'short'],
            'function_typehint_space' => true,
            'method_argument_space' => ['on_multiline' => 'ensure_fully_multiline'],
            'no_empty_statement' => true,
            'no_leading_namespace_whitespace' => true,
            'return_type_declaration' => ['space_before' => 'none'],
            'no_superfluous_phpdoc_tags' => [
                'allow_mixed' => false,
                'allow_unused_params' => false,
                'remove_inheritdoc' => false,
            ],
            'no_unused_imports' => true,
            'ordered_imports' => ['sort_algorithm' => 'alpha'],
            'phpdoc_trim' => true,
            'phpdoc_trim_consecutive_blank_line_separation' => true,
            'ordered_class_elements' => ['order' => ['use_trait', 'constant_public', 'constant_protected', 'constant_private', 'property_public', 'property_protected', 'property_private', 'construct', 'destruct', 'magic', 'phpunit', 'method_public', 'method_protected', 'method_private']],
            'no_blank_lines_after_phpdoc' => true,
            'no_empty_statement' => true,
            'array_indentation' => true,
            'binary_operator_spaces' => [
                'default' => 'single_space'
            ],
            'no_singleline_whitespace_before_semicolons' => true,
            'blank_line_before_statement'=>[
                'statements' => ['break', 'continue', 'declare', 'return', 'throw', 'try']
            ],
            'class_attributes_separation' => [
                'elements' => ['const' => 'one', 'method' => 'one', 'property' => 'one']
            ],
            'combine_consecutive_issets' => true,
        ]
);
