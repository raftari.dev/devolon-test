<?php

return [
    'loaded_successfully' => ':entity loaded to database successfully!',
    'wrong_formatted_json' => 'Error loading fixture. File is not in correct json format',
    'key_is_missing' => 'Error loading fixture. `:key` key is missing from json file.',
    'wrong_key_type' => 'Error loading fixture. `:key` key is not an :type.',
];
