<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Product;

class WarehouseService
{
    private int $highestProfit = 0;

    public function optimizedProductionPlan()
    {
        $products = Product::with('articles:id')->get(['id','name','price'])->toArray();
        $inventory = Article::pluck('stock', 'id')->toArray();

        $result = $this->calculateProfit(0, $products, $inventory);
        $result['plan'] = collect($result['plan'])->map(function ($item, $i) use ($products) {
            return [
                'product' => $products[$i],
                'quantity' => $item,
                'profit' => $products[$i]['price'] * $item,
            ];
        });

        return $result;
    }

    private function removeUsedArticlesFromInventory($product, int $count, array $inventory)
    {
        foreach ($product['articles'] as $article) {
            $inventory[$article['id']] -= $article['pivot']['article_quantity'] * $count;
        }

        return $inventory;
    }

    private function calculateProfit(int $profitSum, array $products, array $inventory)
    {
        if (sizeof($products) == 0) {
            if ($profitSum > $this->highestProfit) {
                $this->highestProfit = $profitSum;
            }

            return ['profit' => $profitSum, 'plan' => []];
        }
        $currentProduct = $products[0];
        $maxPossibleProduct = $this->maxProductCanBeCreated($currentProduct, $inventory);

        $maxProfit = $profitSum;
        $creationPlan = array_fill(0, sizeof($products), 0);
        for ($i = $maxPossibleProduct; $i >= 0; $i--) {
            $profitFromCurrentProduct = $maxPossibleProduct * $currentProduct['price'];
            $inventoryAfterCurrentProduct = $this->removeUsedArticlesFromInventory($currentProduct, $i, $inventory);
            $profitThreshold = $profitFromCurrentProduct + $this->profitThreshold(array_slice($products, 1), $inventoryAfterCurrentProduct);
            if ($profitThreshold < $this->highestProfit) {
                continue;
            }
            $result = $this->calculateProfit($profitSum + $profitFromCurrentProduct, array_slice($products, 1), $inventoryAfterCurrentProduct);
            if ($result['profit'] > $maxProfit) {
                $maxProfit = $result['profit'];
                $creationPlan = [$i, ...$result['plan']];
            }
        }

        return ['profit' => $maxProfit, 'plan' => $creationPlan];
    }

    private function maxProductCanBeCreated($product, array $inventory)
    {
        if (sizeof($product['articles']) == 0) {
            return 0;
        }
        $maxPossible = PHP_INT_MAX;
        foreach ($product['articles'] as $article) {
            $max = floor($inventory[$article['id']] / $article['pivot']['article_quantity']);
            $maxPossible = min($max, $maxPossible);
        }

        return $maxPossible;
    }

    private function profitThreshold(array $products, array $inventory)
    {
        $profit = 0;
        foreach ($products as $product) {
            $profit += $product['price'] * $this->maxProductCanBeCreated($product, $inventory);
        }

        return $profit;
    }
}
