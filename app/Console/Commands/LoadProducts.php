<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class LoadProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:products {fixture}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'load products from a json file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fixture = $this->argument('fixture');

        try {
            $products = $this->getProducts($fixture);
            \DB::transaction(function () use ($products) {
                foreach ($products as $data) {
                    $this->createProduct($data);
                }
            });
        } catch (\Throwable $th) {
            $this->error($th->getMessage());

            return 1;
        }

        $this->info(__('messages.loaded_successfully', ['entity' => 'Products']));

        return 0;
    }

    private function createProduct(array $data)
    {
        $product = Product::create($data);
        if (!empty($data['articles'])) {
            $product->articles()->sync($this->getProductArticles($data['articles']));
        }
    }

    private function getProductArticles(array $articles): array
    {
        $out = [];
        foreach ($articles as $article) {
            $out[$article["id"]] = ["article_quantity" => $article["amount"]];
        }

        return $out;
    }

    private function getProducts($file): array
    {
        $json = file_get_contents($file);
        $jsonData = json_decode($json, true);
        if ($jsonData == null) {
            throw new \Exception(__('messages.wrong_formatted_json'));
        }
        if (!isset($jsonData['products'])) {
            throw new \Exception(__('messages.key_is_missing', ['key' => 'products',]));
        }
        if (!is_array($jsonData['products'])) {
            throw new \Exception(__('messages.wrong_key_type', ['key' => 'products', 'type' => 'array']));
        }

        return $jsonData['products'];
    }
}
