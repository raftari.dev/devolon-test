<?php

namespace App\Console\Commands;

use App\Models\Article;
use Illuminate\Console\Command;

class LoadArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:articles {fixture}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'load articles from a json file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $fixture = $this->argument('fixture');

        try {
            $articles = $this->getArticles($fixture);
            Article::insert($articles);
        } catch (\Throwable $th) {
            $this->error($th->getMessage());

            return 1;
        }

        $this->info(__('messages.loaded_successfully', ['entity' => 'Articles']));

        return 0;
    }

    private function getArticles($file): array
    {
        $json = file_get_contents($file);
        $jsonData = json_decode($json, true);
        if ($jsonData == null) {
            throw new \Exception(__('messages.wrong_formatted_json'));
        }
        if (!isset($jsonData['articles'])) {
            throw new \Exception(__('messages.key_is_missing', ['key' => 'articles']));
        }
        if (!is_array($jsonData['articles'])) {
            throw new \Exception(__('messages.wrong_key_type', ['key' => 'articles', 'type' => 'array']));
        }

        return $jsonData['articles'];
    }
}
