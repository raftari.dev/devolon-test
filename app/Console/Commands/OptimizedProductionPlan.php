<?php

namespace App\Console\Commands;

use App\Services\WarehouseService;
use Illuminate\Console\Command;

class OptimizedProductionPlan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'warehouse:production-plan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(WarehouseService $warehouseService)
    {
        dump($warehouseService->optimizedProductionPlan());

        return 0;
    }
}
