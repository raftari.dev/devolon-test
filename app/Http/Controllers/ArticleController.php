<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleStoreRequest;
use App\Http\Requests\ArticleUpdateRequest;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * @return \App\Http\Resources\ArticleCollection
     */
    public function index(Request $request)
    {
        $articles = Article::all();

        return new ArticleCollection($articles);
    }

    /**
     * @return \App\Http\Resources\ArticleResource
     */
    public function store(ArticleStoreRequest $request)
    {
        $article = Article::create($request->validated());

        return new ArticleResource($article);
    }

    /**
     * @return \App\Http\Resources\ArticleResource
     */
    public function show(Request $request, Article $article)
    {
        return new ArticleResource($article);
    }

    /**
     * @return \App\Http\Resources\ArticleResource
     */
    public function update(ArticleUpdateRequest $request, Article $article)
    {
        $article->update($request->validated());

        return new ArticleResource($article);
    }
}
