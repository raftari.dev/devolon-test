<?php

namespace App\Http\Controllers;

use App\Services\WarehouseService;

class WarehouseController extends Controller
{
    public function __invoke(WarehouseService $warehouseService)
    {
        return $warehouseService->optimizedProductionPlan();
    }
}
