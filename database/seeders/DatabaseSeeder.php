<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Article::factory()->count(5)->create();

        $articles = Article::all();
        Product::factory()->count(5)->create()->each(function ($product) use ($articles) {
            $productArticles = $articles->random(rand(2, 3))->pluck('id');
            $syncData = [];
            foreach ($productArticles as $articleId) {
                $syncData[$articleId] = ['article_quantity' => rand(1, 5)];
            }
            $product->articles()->sync($syncData);
        });
    }
}
